fs = require 'fs'
socket = require 'socket.io'
express = require 'express'

app = express.createServer()
codeData =
  language: 'coffeescript'
  text: ''


app.get '/', (request, response) ->
  fs.readFile 'index.html', (err, contents) ->
    response.writeHead 200,
      'Content-Length': contents.length
      'Content-Type': 'text/html'
    response.end contents

app.use '/scripts', express.static(__dirname + '/scripts')

io = socket.listen app

users = []

emitAddUser = (emitter, user) ->
  emitter.emit 'add user',
    name: user.name

Array::remove = (e) -> @[t..t] = [] if (t = @indexOf(e)) > -1

io.sockets.on 'connection', (client) ->
  console.log 'Client connected...'
  console.log users

  client.on 'join', (name) ->
    @me = {name: name}
    users.push @me
    client.broadcast.emit 'messages',
      from: 'server'
      message: "#{name} has joined the session"
    client.emit 'messages',
      from: 'server'
      message: "Welcome #{name}"
    emitAddUser(client, user) for user in users
    emitAddUser(client.broadcast, @me)
    client.emit 'code change', codeData

  client.on 'code change', (data) ->
    console.log data
    console.log @me
    codeData = data
    client.broadcast.emit 'code change', data

  client.on 'disconnect', () ->
    if @me
      name = @me.name || 'Anonymous'
      users.remove @me
    else
      name = 'Anonymous'
    console.log "#{name} disconnected"
    client.broadcast.emit 'remove user',
      name: name
    client.broadcast.emit 'messages',
      from: 'server',
      message: "#{name} has disconnected."

  client.on 'messages', (data) ->
    console.log data
    client.broadcast.emit 'messages', data
app.configure 'development', () ->
  app.listen(1337)
app.configure 'production', () ->
  app.listen(80)
